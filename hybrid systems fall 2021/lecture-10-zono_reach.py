"""
Zonotope coding (CSE 510)
"""

import numpy as np
from copy import deepcopy

import matplotlib.pyplot as plt

from scipy.linalg import expm

def plot(stateset, color='black', lw=1):
    """plot the set"""

    vert_list = verts(stateset)

    xs = [pt[0] for pt in vert_list]
    ys = [pt[1] for pt in vert_list]

    plt.plot(xs, ys, color, lw=lw)

def verts(stateset, num_directions=50):
    """get vertices of set"""

    result = []

    for theta in np.linspace(0, 2*np.pi, num_directions):
        # optimize the set in angle theta

        vx = np.cos(theta)
        vy = np.sin(theta)

        direction_vec = np.array([[vx, vy]])

        pt = stateset.maximize(direction_vec)

        result.append(pt)

    result.append(result[0])

    return result

class UnitBox:
    """unit box container"""

    def __init__(self):
        pass

    def maximize(self, vec):
        """return point that maximizes in the passed-in direction"""

        #vec = np.array(vec, dtype=float)

        pt = []

        for val in vec[0, :]:
            if val < 0:
                pt.append([-1.0])
            else:
                pt.append([1.0])

        return np.array(pt)

class Zonotope:
    """zonotope container"""

    def __init__(self, center, g_mat):
        self.center = center
        self.g_mat = g_mat

        self.domain = UnitBox()

    def affine_transform(self, mat, vec=None):
        """transform the set by mat*x + vec"""

        self.g_mat = mat @ self.g_mat
        self.center = mat @ self.center

        if vec is not None:
            self.center += vec

    def maximize(self, direction_vec):
        """maximize zonotope"""

        if isinstance(direction_vec, list):
            direction_vec = np.array([direction_vec], dtype=float)

        # convert direction to domain
        domain_dir = direction_vec @ self.g_mat

        domain_pt = self.domain.maximize(domain_dir)

        range_pt = self.g_mat @ domain_pt + self.center

        return range_pt

def main():
    """main entry point"""

    step_size = np.pi / 8
    max_steps = 10
    x_guard = 3.9

    #init: x in [-5, -4], y in [0, 1]
    g_mat = np.array([[0.5, 0.0], [0.0, 0.5]])
    center = np.array([[-4.5], [0.5]])
    init = Zonotope(center, g_mat)
    plot(init, 'black')

    # x' = y, y' = -x 
    a_mat = np.array([[0.0, 1.0], [-1.0, 0.0]])
    ha_sol_mat = expm(a_mat * step_size)

    waiting_list = []
    init_tuple = (init, 'ha', 0)
    waiting_list.append(init_tuple)

    while waiting_list:
        stateset, mode, step = waiting_list.pop()

        while step < max_steps:

            if mode == 'ha':
                min_x = stateset.maximize([-1, 0])[0]
                is_inside_invariant = min_x < x_guard
            else:
                is_inside_invariant = True

            if not is_inside_invariant:
                break

            if mode == 'ha':
                mat = ha_sol_mat
                vec = None
            else:
                assert mode == 'move_right'
                
                mat = np.identity(2)
                vec = np.array([[1.2], [0]])

            stateset.affine_transform(mat, vec)

            color = 'black' if mode == 'ha' else 'r:'
            lw = 1 if mode == 'ha' else 2
            plot(stateset, color, lw=lw)

            step += 1

            # check outgoing transitions
            if mode == 'ha':
                max_x = stateset.maximize([1, 0])[0]

                if max_x >= x_guard:
                    copy_of_set = deepcopy(stateset)
                    tup = (copy_of_set, 'move_right', step)
                    waiting_list.append(tup)

                    plot(copy_of_set, 'r:', lw=2)

    plt.plot([x_guard, x_guard], [-10, 10], 'b--')
            
    plt.xlim([-6, 10])
    plt.ylim([-6, 6])
    plt.show()

if __name__ == "__main__":
    main()
