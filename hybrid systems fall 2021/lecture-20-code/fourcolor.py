"""
four color example in class
"""

from z3 import Solver, Bool, Or, Not, And, sat, Implies

def main():
    """main entry point""" 

    s = Solver()

    colors = ['r', 'g', 'b', 'y']

    v0 = [Bool(f'v0_{c}') for c in colors]
    v1 = [Bool(f'v1_{c}') for c in colors]
    v2 = [Bool(f'v2_{c}') for c in colors]
    v3 = [Bool(f'v3_{c}') for c in colors]
    v4 = [Bool(f'v4_{c}') for c in colors]

    all_vertices = [v0, v1, v2, v3, v4]

    ### at most one color per vertex
    for vertex_vars in all_vertices:
        for color1 in vertex_vars:
            for color2 in vertex_vars:
                if color1 == color2:
                    continue

                # if color1 then not color 2
                expr = Implies(color1, Not(color2))
                s.add(expr)

    ### at least one color per vertex
    for vertex_vars in all_vertices:
        large_disjunction = vertex_vars[0]

        for color in vertex_vars[1:]:
            large_disjunction = Or(large_disjunction, color)

        s.add(large_disjunction)

    ### neighbors should be different colors
    edges = [(0, 1), (0, 2), (0, 3), (0, 4),
             (1, 2), (1, 3), (1, 4),
             (2, 3),
             (3, 4)]

    for src, dest in edges:
        for color1, color2 in zip(all_vertices[src], all_vertices[dest]):
            s.add(Implies(color2, Not(color1)))
            s.add(Implies(color1, Not(color2)))

    #s.add(v0[0])
    
    num_solutions = 0
    while s.check() == sat:
        num_solutions += 1
        print("sat, model:")
        m = s.model()
        #print(f"model: {m}")

        model_expr = None

        for vertex_vars in all_vertices:
            for color in vertex_vars:
                if m[color]:
                    print(color)

                    if model_expr is None:
                        model_expr = color
                    else:
                        model_expr = And(model_expr, color)

        s.add(Not(model_expr))
        print()
                    
    print(f"num solutions: {num_solutions}")

    print("assertions: ")
    for i, formula in enumerate(s.assertions()):
        print(f"{i}: {formula}")

if __name__ == "__main__":
    main()
