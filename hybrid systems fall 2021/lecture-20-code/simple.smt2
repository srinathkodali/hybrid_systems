; Basic Boolean example
;(set-option :print-success false)
(set-logic QF_UF)
(declare-const p Bool)
(declare-const q Bool)

; (op param1 param2 ...)

(assert (and p q)) 

(assert (and (not p) q))



(check-sat)

(get-value (p))
(get-value (q))
(exit)
