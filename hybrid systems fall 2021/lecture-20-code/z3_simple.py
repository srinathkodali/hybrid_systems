'''
simple z3py example
'''

from z3 import Solver, Bool, Or, Not, And, sat

def main():
    'main entry point'

    s = Solver()

    # p and -q
    p = Bool('p')
    q = Bool('q')
    r = Bool('r')
    
    print(p)

    s.add(And(p, Not(q)))
    s.add(Not(p))
    #e1 = And(p, Not(q))
    #e2 = And(q, Not(p))
    #s.add(Or(e1, e2))

    if s.check() == sat:
        print("sat")
        m = s.model()
        print(f"model: {m}")
        
        print(f"p: {m[p]}")
        print(f"q: {m[q]}")
        print(f"r: {m[r]}")
    else:
        print("unsat")

if __name__ == "__main__":
    main()
