"""
CSE 510 - Fall 2021
Simulation with Matrix Exponential
"""

# Class notes:
# Supplemental notes on simulation and reachability
# HW 1 returned. Stats: 2 As, 3 Bs, 2 Cs, some <C
# Project idea due September 29 (Next Wednesday). Need to talk to me about your idea
# (for example, during office hours)
#
# HW 2 will be released this Wednesday) (due 2 weeks later)

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import RK45
from scipy.linalg import expm

def harmonic_oscillator(t, state):
    """returns derivative of harmonic oscillator"""

    x, y = state

    xder = y
    yder = -x

    return xder, yder

def run_euler(der_func, num_steps, h, start):
    """run euler method and return state at each step"""

    state_list = [start.copy()]

    for step_num in range(num_steps):
        cur_time = h * step_num

        cur_point = state_list[-1]

        cur_der = np.array(der_func(cur_time, cur_point), dtype=float)

        # update rule for Euler
        next_point = cur_point + h * cur_der

        state_list.append(next_point)

    return state_list

def run_rk45(der_func, tmax, start):
    """run rk45 simulation"""

    state_list = [start.copy()]
    rk45 = RK45(der_func, 0, start, tmax)

    while abs(rk45.t - tmax) > 1e-6:
        rk45.step()

        t_old = rk45.t_old
        t_cur = rk45.t
        delta_t = t_cur - t_old

        # sample between t_old and t_cur using dense_output()
        interpolation_func = rk45.dense_output()

        for step in range(10):
            sample_time = t_old + step / 10 * delta_t

            intermediate_state = interpolation_func(sample_time)
            state_list.append(intermediate_state)
            

        state_list.append(rk45.y)

    return state_list

def run_expm_ha(num_steps, h, start):
    """simulate using matrix exponential method"""

    state_list = [start.copy()]

    # u' = A u
    # A = [[0 1], [-1, 0]]

    a_mat = np.array([[0.0, 1.0], [-1.0, 0.0]])

    one_step_expm_result = expm(a_mat * h)

    # u(h) = e^{Ah} @ start_vec
    for _ in range(num_steps):

        last_state = state_list[-1]
        next_state = one_step_expm_result @ last_state

        state_list.append(next_state)

    return state_list

def main():
    """main entry point"""

    color_list = ['r-', 'b-', 'go']
    label_list = ['euler', 'rk45', 'expm']

    for index in range(3):
        tmax = 6.0
        start = np.array([-5.0, 0.0])

        h = 0.1
        num_steps = int(round(tmax / h))

        if index == 0:
            results_list = run_euler(harmonic_oscillator, num_steps, h, start)
        elif index == 1:
            results_list = run_rk45(harmonic_oscillator, tmax, start)
        else:
            assert index == 2
            results_list = run_expm_ha(num_steps, h, start)

        # plot result
        xs = [pt[0] for pt in results_list]
        ys = [pt[1] for pt in results_list]

        color = color_list[index]
        label = label_list[index]
        plt.plot(xs, ys, color, label=label)

    plt.legend()
    plt.show()
    #plt.savefig('out.png')

if __name__ == "__main__":
    main()
