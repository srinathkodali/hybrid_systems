"""
CSE 510
Euler sim
"""

import numpy as np

import matplotlib.pyplot as plt

def harmonic_oscillator(t, state):
    """returns derivative of harmonic oscillator"""

    x, y = state

    xder = y
    yder = -x

    return xder, yder

def run_euler(der_func, num_steps, h, start):
    """run euler method and return state at each step"""

    state_list = [start.copy()]

    for step_num in range(num_steps):
        cur_time = h * step_num

        cur_point = state_list[-1]

        cur_der = np.array(der_func(cur_time, cur_point), dtype=float)

        # update rule for Euler
        next_point = cur_point + h * cur_der

        state_list.append(next_point)

    return state_list

def main():
    """main entry point"""

    color_list = ['r-', 'b-']

    for index, h in enumerate([0.1, 0.01]):
        tmax = 6.0
        num_steps = int(round(tmax / h))
        start = np.array([-5.0, 0.0])

        results_list = run_euler(harmonic_oscillator, num_steps, h, start)

        # plot result
        xs = [pt[0] for pt in results_list]
        ys = [pt[1] for pt in results_list]

        color = color_list[index]
        plt.plot(xs, ys, color)

    #plt.show()
    plt.savefig('out.png')

if __name__ == "__main__":
    main()
