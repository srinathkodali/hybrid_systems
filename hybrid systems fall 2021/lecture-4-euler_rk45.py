"""
CSE 510
Euler sim
"""

import numpy as np

import matplotlib.pyplot as plt
from scipy.integrate import RK45

def harmonic_oscillator(t, state):
    """returns derivative of harmonic oscillator"""

    x, y = state

    xder = y
    yder = -x

    return xder, yder

def run_euler(der_func, num_steps, h, start):
    """run euler method and return state at each step"""

    state_list = [start.copy()]

    for step_num in range(num_steps):
        cur_time = h * step_num

        cur_point = state_list[-1]

        cur_der = np.array(der_func(cur_time, cur_point), dtype=float)

        # update rule for Euler
        next_point = cur_point + h * cur_der

        state_list.append(next_point)

    return state_list

def run_rk45(der_func, tmax, start):
    """simulate using rk45 method"""

    state_list = [start.copy()]

    rk45 = RK45(der_func, 0, start, tmax)

    while abs(rk45.t - tmax) > 1e-6:
        rk45.step()

        interpolation_func = rk45.dense_output()
        t_old = rk45.t_old
        t_cur = rk45.t
        delta_t = t_cur - t_old

        for step in range(10):
            t_sample = t_old + delta_t * step / 10

            intermediate_state = interpolation_func(t_sample)

            state_list.append(intermediate_state)

        state_list.append(rk45.y)

    return state_list

def main():
    """main entry point"""

    color_list = ['r-', 'b-', 'g-o']

    for index in range(3):
        tmax = 6.0
        start = np.array([-5.0, 0.0])

        if index == 2:
            results_list = run_rk45(harmonic_oscillator, tmax, start)
        else:
            h_list = [0.1, 0.01]
            h = h_list[index]
            num_steps = int(round(tmax / h))
        
            results_list = run_euler(harmonic_oscillator, num_steps, h, start)

        # plot result
        xs = [pt[0] for pt in results_list]
        ys = [pt[1] for pt in results_list]

        color = color_list[index]
        plt.plot(xs, ys, color)

    plt.show()
    #plt.savefig('out.png')

if __name__ == "__main__":
    main()
